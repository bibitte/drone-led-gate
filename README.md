# Drone LED gate 

Modular drone LED gates system  
![Assembly](/images/animation.gif)  

Video of my first fly on my LED track :   
[![led track demo](/images/video.png)](https://www.youtube.com/watch?v=5zZazUAAnv0)

[[_TOC_]]

# Get STL :

## Download from the latest release

https://gitlab.com/bibitte/drone-led-gate/-/releases

## Download from thingiverse

Coming soon

## Build STL from source:

### prerequisite

#### Windows
Install git https://git-scm.com/download/win

Install cygwin https://cygwin.com/install.html **don't forget to add make module**

Install Open SCAD https://openscad.org/downloads.html

Add open scad installation directory to your path.

#### Linux
Install git, open scad and buildessential

For Debian based distribution :

    sudo apt install git openscad build-essential

### clone repo : 
To clone repository, use this command line :

    git clone https://gitlab.com/bibitte/drone-led-gate.git

### build stl and clean :
(On Windows you need to run this commands with the cygwin terminal).

Go to the drone-led-gate directory 

(if needed replace "drone-led-gate" with the full path, with cygwin C: is on /cygdrive/c/ directory ):

    cd drone-led-gate

To build all STL :

    make

To clean STL directory :

    make clean

# How to print it

## material

To ensure to have easy and good connection It's recommended to print connectors, connector caps and pipe caps with TPU. The flexibility of the TPU help to have good connection even if jacks connector are not perfectly aligned.
For the gate support, the best option is to print it with PLA to have a good rigidity

## Global parameters

This is the parameters that I changed to have best result. You need to test it because it also depends on your printer.
Line width = 0.38 (to have a little overlap)
Wall line count = 3 (increase resistance)
Wall thickness =1.14 (3 lines of 0.38)
Enable support.
Support overhang angle = 60 (minimize support)

## Print connector (Positioning)

There are multiple options to print it, try all and choose the best for your printer

### Parallel to the bed with standard support ( the best result for me but use a lot of support)

This is the method with the best result but that use lot of material and there is support on assembly surface that need to be completely removed.
![standart support](/images/print1.png)  

### Inclined (not perfect but totally usable and use less support)

It's my favorite method because that use less material. 
Place approximately your part with the right angle and use the lay flat function to ensure te part touch the bed with almost 3 points.

![Inclined x4](/images/print2.png) 
![Inclined x3 corner](/images/print2.1.png)  

### Parallel to the bed with tree support (bad result for me, I don't know why)

This is the method with the less amount of material but on my printer I have bad result, but I didn't try to find the great configuration to solve it, maybe you will have the best result than me.

![Tree support](/images/print3.png) 

# How to build it

## Material 

If you don't want to have to customize SCAD or STL files use exactly the same part as me for the jack connectors and use 12-16mm diameter pipes
Ensure to use the same format between female and male jack connectors, for me, I used 5.5x2.1

- Male jack connector  (need 2 per tube , 8 for a gate) I used this one : [mal jack connectors](https://fr.aliexpress.com/item/33030060974.html)
- Female jack connector (the number needed to depend on your connector choice) I used this one : [female jack connectors](https://fr.aliexpress.com/item/33038933019.html)
- Wires. If you want to be able to power multiple gate with one source, you need a long wire. I used this one in 20 AWG [wires](https://fr.aliexpress.com/item/4000689875049.html)
- LED strips. Don't use an addressable LED strip, just one simple LED strip with only one color. I used this one in non-waterproof format : [LED strips](https://fr.aliexpress.com/item/1599651146.html) 
- Pipes. It's the most expensive part of this project. I spend long time to find the cheapest option. If you find a cheaper pipe (and other length with a low price) contact me.
I found a frosted acrylic hard pipe. Usually used for water colling it's not too expensive, it's strong, and the rendering is perfect. I use this one in 12-16mm format : [2pc frosted acrylic hard pipe](https://fr.aliexpress.com/item/1005001698997712.html)

## Tools

You need simple tools to build it
- Soldering iron
- Tin
- Cyanoacrylate glue
- Cutter or wire stripper

## Build connector

To build a connector, you need to follow these steps :

1. Cut the wire :  
For one connector with N connections, you need N-1 couple of wire. For example, if you want to build a connector x4 you need 3 couples of wire.
For all connector wire of 4 cm.
1. Mount the female jack into the connector cap :  
Add glue on the female jack connector thread.  
![Glue female jack](/images/connector_cap_1.jpg)  
Just press to put the female jack connector into the cap.  
![Press female jack](/images/connector_cap_2.jpg)
1. Prepare :  
Tin your wire and your female jack connector
Solder one couple of wires per female jack connector (I used center positive, the long leg is the negative) and keep one connector without wire.  
![Connector caps with wires](/images/connector_1.jpg)
1. Solder connector together :  
Place the first cap in the connector structure and ensure to have the wire that make sure you have the cables coming out through a 90° hole.
Place (not completely) the second connector with cable on this hole and solder the wire of the first connector to this one in parallel (negative with negative and positive with positive)
Place completely this second caps in the connector structure and ensure to have the wires  coming out through a 90° hole like for the first cap.
Repeat it with all the female jack connectors.  
![all connector caps](/images/connector_2.jpg)
1. Test it :  
Before glue it, test it!  
![Test connector](/images/connector_test_1.jpg)  
If all it OK, you may have continuity between all center pin, between all outside ring and no continuity between center pin and outside ring. 
1. Glue it :  
If all tests are ok, take out caps slightly to add a little amount of glue and replace them before glue dries.

## build LED pipe

1. Cut the wire :  
For the pipe cap, you need one couple of wire of 6 cm per cap (2 per pipe).
1. Prepare :
Tin your wire and your male jack connector  
Solder one couple of wires per male jack connector (I used center positive, the long leg is the negative)
1. Mount the jack connector into the pipe cap :  
You need 1 connector, 1 male jack with wire, 1 pipe cap and the press tool.  
![What you need](/images/pipe_cap_1.jpg)  
Place de male jack into the female jack.  
![Mount step 1](/images/pipe_cap_2.jpg)  
Add glue on the male jack thread.  
![Mount step 2](/images/pipe_cap_3.jpg)  
Place the pipe cap and the press tool.  
![Mount step 3](/images/pipe_cap_4.jpg)  
Just press down to put the male jack into the pipe caps  
![Mount step 4](/images/pipe_cap_5.jpg)  
Remove the pipe cap from the connector.
1. Cut the LED strip :  
Cut the LED strip to the pipe length less one section.
1. Solder the LED strip :  
Solder the first side of the LED strip (don't forget heat shrink tube like me :wink: )  
![solder LED strip](/images/pipe_1.jpg)  
Place the LED strip into the pipe  
![solder LED strip](/images/pipe_2.jpg)  
And solder the other side  
![solder LED strip](/images/pipe_3.jpg)  
1. Place the Pipe caps :  
To place definitively the pipe caps, you need to bend the wire. The best way I found is to use flat pliers to form a "S" like that  
![bend the wire](/images/pipe_4.jpg)  
Do it on one side (temporary place the other side without bend wire if you need more length) and push the cap on the pipe.
Do it on the other side.
1. Test it :  
Just use a connector to power it.
Don't forget to test the both side of the pipe.
1. Glue it :  
To secure the pipe cap, you can glue it to the pipe.

# How to use it

## How to assemble it

It's easy you just need to plug tubes on connectors.  
![Assembly](/images/animation.gif)  
You can do every shape you want, like  simple gate, cube, double cube ....

## How power it

To power it, you only need 12v DC on a male jack.
For example, you can use 3S Lipo with a jack adaptor  
![lipo](/images/lipo.jpg)  
You can also 110v/220v -> 12v Power supply.   
I use this one in 200w format : https://fr.aliexpress.com/item/1005002728038351.html  
![power supply](/images/alim.jpg)  
All 12v DC power source can work.  

You can power multiple gates in parallel.
Just plug a new male/male jack between two gates.
The best option to limit amperage into LED strip is to plug all the wires on the same connectors.  
![connector 2 jacks large ](/images/connector large.jpg) 
![2 gates](/images/2_gates.jpg) 
