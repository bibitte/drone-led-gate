include <config.scad>


global_connector_diameter=inner_connector_diameter+4*wall_thickness+2*print_offset_pla;
base_height=global_connector_diameter/2;
outer_pipe_diameter_with_offset=outer_pipe_diameter+2*print_offset_pla;


union(){
    //base  
    difference(){  
        union(){
            
            translate([0,base_height,0])
                cube([support_thickness,support_length-base_height*2,base_height]);
            translate([0,base_height,0])
            rotate([0,90,0])
                cylinder(h=support_thickness,r=base_height);
            translate([0,support_length-base_height,0])
            rotate([0,90,0])
                cylinder(h=support_thickness,r=base_height);
            
            delta_arms= (support_clip_z*2)/sqrt(2);
            translate([0,support_length/2-delta_arms,support_clip_z-delta_arms])
                rotate([45,0,0])
                cube([support_thickness,support_clip_z*2,base_height]);
            translate([0,support_length/2,support_clip_z])
                rotate([-45,0,0])
                cube([support_thickness,support_clip_z*2,base_height]);
            
            delta_arms_edge = (base_height)/sqrt(2);

            
            
            delta_clip = 2* wall_thickness;
            difference(){
                union(){
                    translate([outer_pipe_diameter_with_offset/2+ delta_clip +support_thickness,support_length/2,support_clip_z])
                        cylinder(h=support_clip_height,r=outer_pipe_diameter_with_offset/2+support_thickness);
                                
                    translate([0,support_length/2-delta_arms_edge,support_clip_z])
                        cube([outer_pipe_diameter_with_offset/2+delta_clip,delta_arms_edge*2,support_clip_height]);
                }
                // clip: supress center 
                translate([outer_pipe_diameter_with_offset/2+ delta_clip +support_thickness,support_length/2,support_clip_z-construction_offset])
                    cylinder(h=support_clip_height+construction_offset*2,r=outer_pipe_diameter_with_offset/2);
                // clip: open clip
                translate([outer_pipe_diameter_with_offset*2/3+ delta_clip +support_thickness,support_length/2-outer_pipe_diameter/2-support_thickness-construction_offset,support_clip_z-construction_offset])
                    cube([outer_pipe_diameter_with_offset,outer_pipe_diameter_with_offset+2*support_thickness+2*construction_offset,support_clip_height+construction_offset*2]);
            }
            
            
            //base clip
    translate([0,support_length/2-global_connector_diameter/2-support_thickness,0])
    difference(){
        cube([global_connector_diameter+2*support_thickness,global_connector_diameter+2*support_thickness,global_connector_diameter+2*support_thickness]);
       translate([global_connector_diameter/2+support_thickness,-construction_offset,global_connector_diameter/2+support_thickness])   
            rotate([-90,0,0])    
                cylinder(h=support_length,r=global_connector_diameter/2);   
       translate([global_connector_diameter/2+support_thickness,global_connector_diameter/2+support_thickness,global_connector_diameter/2+support_thickness])   
            rotate([0,90,0])    
                cylinder(h=support_length,r=global_connector_diameter/2);
       translate([global_connector_diameter/2+support_thickness,global_connector_diameter/2+support_thickness,global_connector_diameter/2+support_thickness])   
            rotate([0,0,0])    
                cylinder(h=support_length,r=global_connector_diameter/2);
       translate([global_connector_diameter*2/3+support_thickness,-construction_offset,-construction_offset])
            cube([global_connector_diameter,global_connector_diameter+2*support_thickness+2*construction_offset,global_connector_diameter+2*support_thickness+2*construction_offset]);
        }
        

    }
        
        //suppress arms under the base
        translate([-construction_offset,-construction_offset,-support_clip_z])
         cube([support_thickness+2*construction_offset,support_length+2*construction_offset,support_clip_z]);
    }
}
