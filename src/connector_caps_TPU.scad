include <config.scad>

difference(){
    union(){
       //rounded edge
        rotate_extrude()
translate([thread_cylinder_inner_diameter/2+print_offset_tpu,0])
        union(){
            square([inner_connector_diameter/2-thread_cylinder_inner_diameter/2-print_offset_tpu,jack_support_thickness/2]);
            translate ([0,jack_support_thickness/2])
            square([inner_connector_diameter/2-thread_cylinder_inner_diameter/2-print_offset_tpu+wall_thickness,jack_support_thickness/2]);
            intersection(){
                square([inner_connector_diameter/2-thread_cylinder_inner_diameter/2-print_offset_tpu+wall_thickness,jack_support_thickness]);
                translate([inner_connector_diameter/2-thread_cylinder_inner_diameter/2-print_offset_tpu,jack_support_thickness/2])
                circle(r=jack_support_thickness/2);
            }
        }
        translate([0,0,jack_support_thickness])
            difference(){
                cylinder(h=connector_caps_length+print_offset_tpu,r=inner_connector_diameter/2-print_offset_tpu);
                translate([0,0,-construction_offset])
                cylinder(h=connector_caps_length+print_offset_tpu+2*construction_offset,r=inner_connector_diameter/2-wall_thickness-print_offset_tpu);
        }
        translate([0,0,jack_support_thickness])
        difference(){
            cylinder(h=female_thread_height,r=thread_cylinder_inner_diameter/2+wall_thread_thickness);
            translate([0,0,-construction_offset])
            cylinder(h=female_thread_height+2*construction_offset,r=thread_cylinder_inner_diameter/2);
        }
        
        
 
        
        
    }
    
}  







