include <config.scad>

union(){
    difference(){
        cylinder(h=pipe_caps_length,r=outer_pipe_diameter/2+print_offset_tpu+wall_thickness);
        translate([0,0,-construction_offset])
        cylinder(h=pipe_caps_length+2*construction_offset,r=outer_pipe_diameter/2+print_offset_tpu);
    }
    
    translate([0,0,pipe_caps_length])
    difference(){
        cylinder(h=jack_support_thickness,r=max(inner_connector_diameter/2+2*wall_thickness+print_offset_tpu,outer_pipe_diameter/2+wall_thickness+print_offset_tpu));
        translate([0,0,-construction_offset])
        cylinder(h=jack_support_thickness+2*construction_offset,r=thread_cylinder_inner_diameter/2+print_offset_tpu);
    }
    
    translate([0,0,pipe_caps_length+jack_support_thickness])
    difference(){
        cylinder(h=male_jack_length+female_jack_thickness-print_offset_tpu,r=inner_connector_diameter/2+2*wall_thickness+print_offset_tpu);
        translate([0,0,-construction_offset])
        cylinder(h=male_jack_length+female_jack_thickness-print_offset_tpu+2*construction_offset,r=inner_connector_diameter/2+wall_thickness+print_offset_tpu);
    }
    translate([0,0,pipe_caps_length-male_thread_height])
    difference(){
        cylinder(h=male_thread_height,r=thread_cylinder_inner_diameter/2+wall_thread_thickness);
        translate([0,0,-construction_offset])
        cylinder(h=male_thread_height+2*construction_offset,r=thread_cylinder_inner_diameter/2);
    }
}


