use <base_connector.scad>

union(){
    base_connector();
    rotate([90,0,0])
    base_connector();
    rotate([270,0,0])
    base_connector();
    
    rotate([0,0,180])
    connexion_filler();
    rotate([90,0,180])
    connexion_filler();
    rotate([270,0,180])
    connexion_filler();
    

    rotate([0,90,90])
    connexion_filler();
    rotate([0,90,270])
    connexion_filler();
 
    connexion_filler();
    rotate([90,0,0])
    connexion_filler();
    rotate([270,0,0])
    connexion_filler();
    
    rotate([90,0,180])
    connexion_filler2();
    rotate([0,90,270])
    connexion_filler2();
    
    rotate([90,0,0])
    connexion_filler2();
    rotate([0,90,90])
    connexion_filler2();
    
}