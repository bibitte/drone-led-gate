include <config.scad>

difference(){
    union(){
        cylinder(h=pipe_caps_length-male_thread_height,r=thread_cylinder_inner_diameter/2+wall_thread_thickness);
        translate([0,0,(pipe_caps_length-male_thread_height)/2])
        cylinder(h=(pipe_caps_length-male_thread_height)/2,r=outer_pipe_diameter/2-print_offset_tpu);
        translate([0,0,pipe_caps_length-male_thread_height])
        cylinder(h=wall_thickness*2,r=outer_pipe_diameter/2+wall_thickness*2);
    }
    translate([0,0,-construction_offset])
    cylinder(h=pipe_caps_length+wall_thickness*2-male_thread_height+2*construction_offset,r=thread_cylinder_inner_diameter/2);
}