include <config.scad>
 
 
module cutter_cube(){
    translate([-construction_offset,-(inner_connector_diameter+wall_thickness*2*2+construction_offset)/2,0])
    rotate([0,45,0])
    cube(inner_connector_diameter+wall_thickness*2+2*construction_offset);
}


module base_connector(){
    difference(){
        union(){
            cylinder(h=connector_length+male_jack_length-jack_support_thickness,r=inner_connector_diameter/2+wall_thickness);
            cylinder(h=connector_length,r=inner_connector_diameter/2+2*wall_thickness);
        }
        translate([0,0,-construction_offset])
        cylinder(h=connector_length+male_jack_length-jack_support_thickness+2*construction_offset,r=inner_connector_diameter/2);
                
        cutter_cube();
        rotate([0,0,90])
        cutter_cube();
        rotate([0,0,180])
        cutter_cube();
        rotate([0,0,270])
        cutter_cube();
        
    }

    
}

module connexion_filler() {
    difference(){
        intersection(){
            cylinder(h=connector_length,r=inner_connector_diameter/2+2*wall_thickness);
            cutter_cube();
        }
        translate([0,0,-construction_offset])
        cylinder(h=connector_length+2*construction_offset,r=inner_connector_diameter/2);
        rotate([0,0,90])
        cutter_cube();
        rotate([0,0,270])
        cutter_cube();
        
    }
}
module connexion_filler2() {
    intersection(){
        difference(){
            intersection(){
                cylinder(h=connector_length,r=inner_connector_diameter/2+2*wall_thickness);
                cutter_cube();
            }
            translate([0,0,-construction_offset])
            cylinder(h=connector_length+2*construction_offset,r=inner_connector_diameter/2);
            rotate([0,0,90])
            cutter_cube();
        }
     rotate([0,0,270])
        cutter_cube();
    }
}

module corner_filler(){
    intersection(){
        difference(){
            sphere(r=inner_connector_diameter/2+2*wall_thickness);
            sphere(r=inner_connector_diameter/2);
        }
        translate([-construction_offset,-inner_connector_diameter-wall_thickness*2*2,-inner_connector_diameter-wall_thickness*2*2])
    cube(inner_connector_diameter+wall_thickness*2*2+construction_offset);
    }
    
}



base_connector();
connexion_filler(); 
connexion_filler2(); 
corner_filler();



//cutter_cube();