use <base_connector.scad>

union(){
    base_connector();
    rotate([90,0,0])
    base_connector();
    rotate([180,0,0])
    base_connector();
    rotate([270,0,0])
    base_connector();
    rotate([0,90,0])
    base_connector();
    rotate([0,270,0])
    base_connector();
}