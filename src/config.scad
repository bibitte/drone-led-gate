/*[Rendering]*/
//Minimum angle for a fragment
$fa = 1;
//minimum size of a fragment: 
$fs = 0.4;

/*[Pipe dimensions]*/
//outer pipe diameter
outer_pipe_diameter=16;
//length of the pipe that covered by the caps
pipe_caps_length=10;


/*[Connector dimensions]*/
//length of one side of the connector (jack length not included)
connector_length=15;
//length of the caps (support thickness not included)
connector_caps_length=5;
// inner diameter of the connector
inner_connector_diameter=16;


/*[support dimensions]*/
//length of the support
support_length=270;

//thickness of the support
support_thickness=2;
//support clip height
support_clip_height=7;

//support clip position on axis
support_clip_z=70;


/*[Global dimensions]*/
//length of the jack
male_jack_length=10;
//female jack thickness (from support to the base of the plugged male)
female_jack_thickness=5;
//thickness of the jack connector support plate
jack_support_thickness=2;
//wall thickness (thickness of the connector is 2*wall_thinkess)
wall_thickness=1;
//print offest to facilitate the assembly for TPU(for me is 0 on TPU)
print_offset_tpu=0;
//print offest to facilitate the assembly for PLA(for me is 0.2 on PLA)
print_offset_pla=0.2;
//offest used for the construction to avoid non-manifold issue
construction_offset=0.01;
//thread cylinder diameter (for the thread the print offset is not added) 
thread_cylinder_inner_diameter=7.6;
//male thread hight (doesn't include jack_support_thickness)
male_thread_height=5-jack_support_thickness;
//female thread hight (doesn't include jack_support_thickness)
female_thread_height=7-jack_support_thickness;
//wall thread thickness (use printer wall size*2)
wall_thread_thickness= 2.28;



