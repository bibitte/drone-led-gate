SRC_DIR=src
OUTPUT_DIR=stl
SRC=$(wildcard $(SRC_DIR)/connector_*.scad) $(SRC_DIR)/pipe_caps_TPU.scad $(SRC_DIR)/press_tool.scad $(SRC_DIR)/support_PLA.scad
STL=$(SRC:$(SRC_DIR)/%.scad=$(OUTPUT_DIR)/%.stl)

all : $(STL)

$(OUTPUT_DIR)/%.stl : $(SRC_DIR)/%.scad $(OUTPUT_DIR)
	openscad -o $@ $<

$(OUTPUT_DIR) :
	mkdir -p $(OUTPUT_DIR)

clean : 
	rm -f $(STL)
	rmdir $(OUTPUT_DIR)


